<?php

/*
TECHNICAL TEST

PURPOSE: It is to test your understanding on object oriented programming
WHAT TO DO: Normalize the code as much as possible so that it is scalable in the future if there were more to add on for similar functions
HINT: U may need to normalize a few iteration
*/
Class game_states{
    
    public function prepare_markets()
    {
        $match_summary = $match['pbp_stats']['match_summary'];
        
        //get first, I am not sure why some key are 'first', some are 'firsts'
        //but i assumed it is all 'first' as coding consistency
        $this->get_first($match_summary['first']);
        
        //get objective events
        $this->get_objective_events($match_summary['objective_events']);
        
        //get blue roster
        $this->get_blue_roster($match_summary);
    }
    
    private function get_first($match_summary_first){
        
        foreach($match_summary_first as $key=>$value){
            
            $player_id = $value['player_id'];
                
                
            if($key == "first_blood"){
                $strKey.="TEAM TO DRAW FIRST BLOOD";
                
            }else if($key == "first_tower"){
                $strKey.="DESTROY FIRST TOWER";
                
            }else if($key == "first_baron"){
                $strKey.="TO KILL FIRST BARON";
                
            }else if($key == "first_rift_herald"){
                $strKey.="TO KILL FIRST RIFT HERALD";
            }
            
            if ($player_id){
                
                //set result if there is player id
                $this->set_result($strKey,$this->get_winning_team($player_id));
            }
        }//
    }
    
    
    private function get_winning_team($player_id){
        
        $team_id = $this->team['home']['id'];
        if (!$this->is_player_home_team($player_id))
        {
            $team_id = $this->team['away']['id'];
        }
        
        return $team_id;
    }
    
    
    private function get_blue_roster($match_summary){
        
        //total_gold_diff
        $blue_roster_player_id = $match_summary['blue_roster']['players'][0]['player_id'];

        if (!$blue_roster_player_id)
        {
            return;
        }

        list($home, $away) = $this->get_home_away_team($blue_roster_player_id);

        $home_gold_earned = $this->sum_by_array_key($match_summary[$home]['players'], 'gold_earned');

        $away_gold_earned = $this->sum_by_array_key($match_summary[$away]['players'], 'gold_earned');

        $total_gold_diff = abs(round(($home_gold_earned - $away_gold_earned) / 1000));
        
        $this->set_result("TOTAL GOLD DIFFERENCE OVER/UNDER (IN THOUSANDS)",$total_gold_diff);
        //total_gold_diff
        
        //highest gold
        $team_id = $this->team['home']['id'];
        if ($away_gold_earned > $home_gold_earned)
        {
            $team_id = $this->team['away']['id'];
        }
        $this->set_result("WHICH TEAM HAVE HIGHEST GOLD",$team_id);
        //highest gold
        
        //most assist point
        $home_assists = $this->sum_by_array_key($match_summary[$home]['players'], 'assists');
        
        $away_assists = $this->sum_by_array_key($match_summary[$away]['players'], 'assists');

        $team_id = $this->team['home']['id'];
        if ($away_assists > $home_assists)
        {
            $team_id = $this->team['away']['id'];
        }
        $this->set_result("TEAM WITH MOST ASSIST POINT",$team_id);
        //most assist point
        
    }
    
    private function sum_by_array_key($aryPlayers, $key){
        
        $sum = array_sum(
            array_column($aryPlayers, $key)
        );
        
        return $sum;
    }
    
    private function get_objective_events($objective_events){
        
        /*all of the slain functions are the same
          only difference is the match_summary key ['objective_events']['dragons'] & ['objective_events']['barons']
          and result key
          not sure how does common_event_score_line differeniate the score
          settle_map_first_tower_destroyed_location && settle_map_first_inhibitor_destroyed_location functions
          doesn't make sense. I will just normalized it based on what I see*/
        
        
        foreach($objective_events as $key=>$value){
            
            $strObj = "";
            
            //ignore if key is towers and inhibitors
            if($key != "towers" && $key != "inhibitors"){
                
                $events = $value;
                $scoreline = $this->common_event_score_line($events);
                
                if($key == "dragons"){
                    $strObj = "DRAGON";
                    
                }else if($key == "barons"){
                    $strObj = "BARON";
                }
                
                $this->set_result("TOTAL {$strObj} SLAIN HANDICAP", $scoreline);
                $this->set_result("TOTAL {$strObj} SLAIN OVER/UNDER", $scoreline);
                $this->set_result("TOTAL {$strObj} ODD/EVEN", $scoreline);
                
            }else{
                if($key == "towers"){
                    $location =$objective_events['towers'][0]['lane'];
                    $strObj = "TOWER";
                    
                }else if($key == "inhibitors"){
                   $location = $objective_events['inhibitors'][0]['lane'];
                   $strObj = "INHIBITOR";
                }
                
                if($location){
                    $strKey.="FIRST {$strObj} DESTROYED LOCATION";
                    $this->set_result($strKey, $this->get_winning_team($player_id));
                }
            }
        }//foreach
        
    }
    
    private function set_result($text, $value){
        
        $strKey = "{$this->mapNumStr}";
        
        $strKey.=" ".$text;
        
        $this->result[$strKey] = $value;
    }
}
?>

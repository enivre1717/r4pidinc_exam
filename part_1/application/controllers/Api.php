<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	
	public function organization()
	{
		$this->load->model('employees');
                
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($this->employees->get_employees_under()));
                
        }
        
        public function offices()
	{
		$this->load->model('offices');
                
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($this->offices->get_offices()));
                
        }
        
        public function sales_report($employeeNumber = NULL)
	{
		$this->load->model('reports');
                
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($this->reports->sales_report()));
        }
}

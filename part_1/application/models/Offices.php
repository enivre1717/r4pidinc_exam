<?php
class Offices extends CI_Model {
        
        public $officeCode;
        public $city;
        public $phone;
        public $addressLine1;
        public $addressLine2;
        public $state;
        public $country;
        public $postalCode;
        public $territory;

        public function get_offices()
        {
            $query = $this->db->get('offices');
            
            $i = 0;
            foreach($query->result() as $row){
                
                $aryOffices[$i] = array();
                $aryOffices[$i]["officeCode"] = $row->officeCode;
                $aryOffices[$i]["city"] = $row->city;
                
                //check if there is employee belong to the office
                $employeeUnderQuery = $this->db->where(array("officeCode" => $row->officeCode))->from("employees");

                if($employeeUnderQuery->count_all_results() > 0){
                    
                    $this->load->model('employees');
                    
                    $aryOffices[$i]["employees"] = $this->employees->get_employees_by_office($row->officeCode);
                }

                $i++;
            }//foreach
            
            return $aryOffices;
        }
        
}

<?php
class Reports extends CI_Model {
        
        public function sales_report($employeeNumber = NULL){
            
            
            $strEmployeesFilter = "";
            $strSep =" WHERE ";
            
            $strEmployeesFilter.=$strSep."employees.reportsTo IS NOT NULL";
            $strSep =" AND ";
            
            $strEmployeesFilter.=$strSep."employees.jobTitle = 'Sales Rep'";
            $strSep =" AND ";
            
            $strOrdersFilter ="";
            $strSep2 =" WHERE ";
            
            $strOrdersFilter.=$strSep."employees.reportsTo IS NOT NULL";
            $strSep =" AND ";
            
            $strOrdersFilter.=$strSep."employees.jobTitle = 'Sales Rep'";
            $strSep =" AND ";
            
            
            $query = $this->db->query("SELECT employeeNumber,  firstName, lastName, jobTitle, officeCode, city, 

ROUND(IFNULL(SUM(productTotalSales),0),2) AS totalSales,
ROUND(IFNULL(SUM(productCommission),0),2) AS totalCommission

FROM (

SELECT 
employees.employeeNumber, employees.firstName, employees.lastName, employees.jobTitle, employees.officeCode, offices.city,
0 AS productTotalSales,
0 AS productCommission

FROM employees 
INNER JOIN offices ON (employees.officeCode = offices.officeCode)
".$strEmployeesFilter."

UNION

SELECT 
employees.employeeNumber, employees.firstName, employees.lastName, employees.jobTitle, employees.officeCode, offices.city,
SUM(orderdetails.quantityOrdered*priceEach) AS productTotalSales,
SUM((orderdetails.quantityOrdered*priceEach)/ CAST(SUBSTRING_INDEX(products.productScale,':', -1) AS INT)) AS productCommission

FROM employees 
INNER JOIN offices ON (employees.officeCode = offices.officeCode)

LEFT JOIN customers ON (employees.employeeNumber = customers.salesRepEmployeeNumber)
LEFT JOIN orders ON (customers.customerNumber = orders.customerNumber)
LEFT JOIN orderdetails ON (orders.orderNumber = orderdetails.orderNumber)
LEFT JOIN products ON (orderdetails.productCode = products.productCode)
".$strOrdersFilter."
GROUP BY employees.employeeNumber, orders.orderNumber,orderdetails.productCode

) AS sales_report
GROUP BY employeeNumber");
            
            
            $i = 0;
            
            foreach($query->result() as $row){
                
                $arySalesReport[$i] = array();
                $arySalesReport[$i]["employeeNumber"] = $row->employeeNumber;
                $arySalesReport[$i]["name"] = $row->firstName." ".$row->lastName;
                $arySalesReport[$i]["jobTitle"] = $row->jobTitle;
                $arySalesReport[$i]["officeCode"] = $row->officeCode;
                $arySalesReport[$i]["city"] = $row->city;
                $arySalesReport[$i]["totalCommision"] = $row->totalCommission;
                $arySalesReport[$i]["totalSales"] = $row->totalSales;
                
                if($row->totalSales > 0){
                    $arySalesReport[$i]["productLines"] = self::productlines_sales_by_employee($row->employeeNumber);
                }
                
                $i++;
                
            }//foreach
            
            
            
            return $arySalesReport;
        }//sales_report
        
        public function productlines_sales_by_employee($employeeNumber){
            
            
            $query = $this->db->query("SELECT 
products.productLine, products.productDescription,
SUM(orderdetails.quantityOrdered*priceEach) AS productTotalSales,
SUM((orderdetails.quantityOrdered*priceEach)/ CAST(SUBSTRING_INDEX(products.productScale, ':', -1) AS INT)) AS productCommission,
SUM(orderdetails.quantityOrdered) AS totalQuantity

FROM employees 
INNER JOIN offices ON (employees.officeCode = offices.officeCode)

LEFT JOIN customers ON (employees.employeeNumber = customers.salesRepEmployeeNumber)
LEFT JOIN orders ON (customers.customerNumber = orders.customerNumber)
LEFT JOIN orderdetails ON (orders.orderNumber = orderdetails.orderNumber)
LEFT JOIN products ON (orderdetails.productCode = products.productCode)
WHERE employees.employeeNumber=".$employeeNumber."
GROUP BY orders.orderNumber,products.productCode");
            
            $i=0;
            foreach($query->result() as $row){
                
                $aryProductlines[$i] = array();
                $aryProductlines[$i]["productLine"] = $row->productLine;
                $aryProductlines[$i]["textDescription"] = $row->productDescription;
                $aryProductlines[$i]["commission"] = $row->productCommission;
                $aryProductlines[$i]["quantity"] = $row->totalQuantity;
                $aryProductlines[$i]["sales"] = $row->productTotalSales;
                
                $i++;
            }//foreach
            
            return $aryProductlines;
        }//productlines_sales_by_employee
}

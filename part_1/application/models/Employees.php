<?php
class Employees extends CI_Model {
        
        public $employeeNumber;
        public $lastName;
        public $firstName;
        public $extension;
        public $email;
        public $officeCode;
        public $reportsTo;
        public $jobTitle;

        public function get_employees_under($employeeNum = NULL, $level = 0)
        {
            if($level == 0){
                $query = $this->db->get('employees');
            }
            else{
                
                $query = $this->db->where(array("reportsTo" => $employeeNum))->from("employees")->get();
               
            }//if
            
            $i = 0;
            foreach($query->result() as $row){
                
                $aryEmployees[$i] = array();
                $aryEmployees[$i]["employeeNumber"] = $row->employeeNumber;
                $aryEmployees[$i]["name"] = $row->firstName ." ". $row->lastName;
                $aryEmployees[$i]["jobTitle"] = $row->jobTitle;

                //check if any employee reports to him/her

                $employeeUnderQuery = $this->db->where(array("reportsTo" => $row->employeeNumber))->from("employees");

                if($employeeUnderQuery->count_all_results() > 0){

                    $aryEmployees[$i]["employeeUnder"] = self::get_employees_under($row->employeeNumber, $level+1);
                }

                $i++;
            }//foreach
            
            return $aryEmployees;
        }//get_employees_under
        
        public function get_employees_by_office($officeCode){
            
            $query = $this->db->where(array("officeCode" => $officeCode))->from("employees")->get();
            
            $i = 0;
            foreach($query->result() as $row){
                
                $aryEmployees[$i] = array();
                $aryEmployees[$i]["employeeNumber"] = $row->employeeNumber;
                $aryEmployees[$i]["name"] = $row->firstName ." ". $row->lastName;
                $aryEmployees[$i]["jobTitle"] = $row->jobTitle;
                
                $i++;
                
            }//foreach
            
            return $aryEmployees;
        }//get_employees_by_office
}
